# Bioinformatics - FER 19./20.

Link to course - https://www.fer.unizg.hr/predmet/bio

Reference paper - Inducing the LCParray (Fischer, 2011) (http://arxiv.org/pdf/1101.3448.pdf)

Original implementation - http://algo2.iti.kit.edu/english/1828.php

## How to compile
Position to project root and run `make`.

## How to run
Command for running is `./main file_name`.
For example, `./main testfiles/exp1.txt`.

## How to run tests
Command for running tests is `make check`.

## How to clean executable files
Command for cleaning executable files is `make clean`.
