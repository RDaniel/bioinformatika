#include <iostream>
#include "sais.h"
#include <stdlib.h>


/**
	Luka Banović
*/
int main(int argc, char **argv) {
    FILE *fp;
    long n = 0;

    fp = fopen(argv[1], "rb");
    if (fseek(fp, 0, SEEK_END) == 0) {
        n = ftell(fp);
        rewind(fp);
    }

    char* Schar = (char*) malloc(n * sizeof(char));
    int* S = (int* ) malloc(n * sizeof(int));
    int* SA = (int* ) malloc(n * sizeof(int));
    int* LCP = (int* ) malloc(n * sizeof(int));

    fread(Schar, sizeof(char), n, fp);

    int bucket_size = 0;

    for (long i = 0; i < n; i++) {
        S[i] = Schar[i];
        if(S[i] > bucket_size) bucket_size = S[i];
    }

    free(Schar);

    for (int i = 0; i < n; i++) {
        LCP[i] = -2;
    }

    int calculate_lcp = 1;
	int start = clock();
	SA_IS(S, SA, LCP, n, bucket_size + 1, calculate_lcp);
	int finish = clock();
    printf("Time: %f\n", (finish - start) / (double) CLOCKS_PER_SEC);

    int i, l, counter;
    counter = 0;
    for (i = 1; i < n; ++i) {
        l = 0;
        while (S[SA[i] + l] == S[SA[i - 1] + l]) ++l;
        if (l != LCP[i]) {
            counter++;
            printf("Error at position %i\n", i);
            printf("%i vs. %i\n", l, LCP[i]);
        }
    }

    free(S);
    free(LCP);
    free(SA);

    return 0;
}
