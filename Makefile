CXX = g++
CFLAGS = -ffast-math -funroll-loops -DNDEBUG
CPPFLAGS = -Wall -DNDEBUG -pthread
CXXFLAGS += -g
TESTFLAGS = -I$(FUSED_GTEST_DIR)

defalut: main

FUSED_GTEST_DIR = test

FUSED_GTEST_H = $(FUSED_GTEST_DIR)/gtest/gtest.h
FUSED_GTEST_ALL_CC = $(FUSED_GTEST_DIR)/gtest/gtest-all.cc

GTEST_MAIN_CC = lib/googletest/src/gtest_main.cc

$(FUSED_GTEST_H) :
	lib/googletest/scripts/fuse_gtest_files.py $(FUSED_GTEST_DIR)

$(FUSED_GTEST_ALL_CC) :
	lib/googletest/scripts/fuse_gtest_files.py $(FUSED_GTEST_DIR)

check : unit_tests
	./unit_tests

main: main.o sais.o
	$(CXX) main.o sais.o -o main

main.o: main.cpp sais.h
	$(CXX) $(CFLAGS) $(CPPFLAGS) -c main.cpp

sais.o: sais.cpp sais.h
	$(CXX) $(CFLAGS) $(CPPFLAGS) -c sais.cpp

clean:
	$(RM) main unit_tests *.o *~

gtest-all.o : $(FUSED_GTEST_H) $(FUSED_GTEST_ALL_CC)
	$(CXX) $(CPPFLAGS) $(CXXFLAGS) $(TESTFLAGS) -c $(FUSED_GTEST_DIR)/gtest/gtest-all.cc

gtest_main.o : $(FUSED_GTEST_H) $(GTEST_MAIN_CC)
	$(CXX) $(CPPFLAGS) $(CXXFLAGS) $(TESTFLAGS) -c $(GTEST_MAIN_CC)

unit_tests.o : $(FUSED_GTEST_DIR)/unit_tests.cpp sais.h $(FUSED_GTEST_H)
	$(CXX) $(CPPFLAGS) $(CXXFLAGS) $(TESTFLAGS) -c $(FUSED_GTEST_DIR)/unit_tests.cpp

unit_tests : sais.o unit_tests.o gtest-all.o gtest_main.o
	$(CXX) $(CPPFLAGS) $(CXXFLAGS) $(TESTFLAGS) $^ -o $@
