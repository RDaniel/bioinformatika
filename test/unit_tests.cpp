#include "gtest/gtest.h"
#include "../sais.h"

TEST(assign_type_tests, simple_example) {
    const int S[] = {'C', 'A', 'T', 'G', 'T', 'G', 'C', 'T', 'T', 'A', 'G', 'C', 'A', '$'};
    char real_t[] = {L_TYPE, S_TYPE, L_TYPE, S_TYPE, L_TYPE, L_TYPE, S_TYPE,  L_TYPE,  L_TYPE, S_TYPE, L_TYPE, L_TYPE, L_TYPE, S_TYPE};
    int n = sizeof(S) / sizeof(int);
    char t[n];

    assign_type(S, n, t);

    for(int i = 0; i < n; i++) {
        EXPECT_EQ(real_t[i], t[i]);
    }
}

TEST(set_buckets_indexes_tests, set_to_end_simple_example) {
    const int S[] = {'C', 'A', 'T', 'G', 'T', 'G', 'C', 'T', 'T', 'A', 'G', 'C', 'A', '$'};
    int n = sizeof(S) / sizeof(int);

    int bucket_size = 0;

    for (long i = 0; i < n; i++) {
        if(S[i] > bucket_size) bucket_size = S[i];
    }

    int real_B[++bucket_size];

    for(int i = 0; i < bucket_size; i++) {
        real_B[i] = -1;
    }

    real_B['$'] = 0;
    real_B['A'] = 3;
    real_B['C'] = 6;
    real_B['G'] = 9;
    real_B['T'] = 13;

    int B[bucket_size];

    set_buckets_indexes(S, B, n, bucket_size, true);

    for(int i = 0; i < bucket_size; i++) {
        ASSERT_EQ(real_B[i], B[i]);
    }
}

TEST(set_buckets_indexes_tests, set_to_beginning_simple_example) {
    const int S[] = {'C', 'A', 'T', 'G', 'T', 'G', 'C', 'T', 'T', 'A', 'G', 'C', 'A', '$'};
    int n = sizeof(S) / sizeof(int);

    int bucket_size = 0;

    for (long i = 0; i < n; i++) {
        if (S[i] > bucket_size) bucket_size = S[i];
    }

    int real_B[++bucket_size];

    for(int i = 0; i < bucket_size; i++) {
        real_B[i] = -1;
    }

    real_B['$'] = 0;
    real_B['A'] = 1;
    real_B['C'] = 4;
    real_B['G'] = 7;
    real_B['T'] = 10;

    int B[bucket_size];

    set_buckets_indexes(S, B, n, bucket_size, false);

    for(int i = 0; i < bucket_size; i++) {
        ASSERT_EQ(real_B[i], B[i]);
    }
}


TEST(induced_sorting_tests, simple_example) {
    const int S[] = {'C', 'A', 'T', 'G', 'T', 'G', 'C', 'T', 'T', 'A', 'G', 'C', 'A', '$'};
    char t[] = {L_TYPE, S_TYPE, L_TYPE, S_TYPE, L_TYPE, L_TYPE, S_TYPE,  L_TYPE,  L_TYPE, S_TYPE, L_TYPE, L_TYPE, L_TYPE, S_TYPE};
    int real_SA[] = {13, 12, 9, 1, 11, 0, 6, 10, 5, 3, 8, 4, 2, 7};

    int n = sizeof(S) / sizeof(int);

    int bucket_size = 0;

    for (long i = 0; i < n; i++) {
        if (S[i] > bucket_size) bucket_size = S[i];
    }

    int B[++bucket_size];
    int SA[n];

    induced_sorting(S, SA, t, n, B, bucket_size);

    for(int i = 0; i < n; i++) {
        ASSERT_EQ(real_SA[i], SA[i]);
    }
}

TEST(determine_lms_names_tests, simple_example) {
    const int S[] = {'C', 'A', 'T', 'G', 'T', 'G', 'C', 'T', 'T', 'A', 'G', 'C', 'A', '$'};
    char t[] = {L_TYPE, S_TYPE, L_TYPE, S_TYPE, L_TYPE, L_TYPE, S_TYPE,  L_TYPE,  L_TYPE, S_TYPE, L_TYPE, L_TYPE, L_TYPE, S_TYPE};
    int real_S1[] = {2, 4, 3, 1, 0};

    int n = sizeof(S) / sizeof(int);
    int n1 = sizeof(real_S1) / sizeof(int);
    int SA[] = {13, 12, 9, 1, 11, 0, 6, 10, 5, 3, 8, 4, 2, 7};

    ASSERT_EQ(n1 - 1, determine_lms_names(t, S, SA, n));

    for(int i = 0; i < n1; i++) {
        ASSERT_EQ(real_S1[i], SA[n - n1 + i]);
    }
}

TEST(determine_SA_from_SA1_tests, simple_example) {
    const int S[] = {'C', 'A', 'T', 'G', 'T', 'G', 'C', 'T', 'T', 'A', 'G', 'C', 'A', '$'};
    char t[] = {L_TYPE, S_TYPE, L_TYPE, S_TYPE, L_TYPE, L_TYPE, S_TYPE,  L_TYPE,  L_TYPE, S_TYPE, L_TYPE, L_TYPE, L_TYPE, S_TYPE};
    int SA[] = {4, 0, 1, 3, 2, 1, 2, -1, 2, 4, 3, 1, 0};
    int P[] = {1, 3, 6, 9, 13};
    int real_SA[] = {13, 12, 9, 1, 11, 0, 6, 10, 5, 3, 8, 4, 2, 7};

    int n = sizeof(S) / sizeof(int);
    int n1 = sizeof(P) / sizeof(int);

    int *SA1 = SA;
    int *S1 = SA + n - n1;

    int bucket_size = 0;

    for (long i = 0; i < n; i++) {
        if(S[i] > bucket_size) bucket_size = S[i];
    }

    int B[++bucket_size];

    determine_SA_from_SA1(S, SA, S1, SA1, B, t, n, n1, bucket_size);

    for(int i = 0; i < n; i++) {
        ASSERT_EQ(real_SA[i], SA[i]);
    }
}

TEST(induced_sorting_with_lcp_tests, simple_example) {
    const int S[] = {'C', 'A', 'T', 'G', 'T', 'G', 'C', 'T', 'T', 'A', 'G', 'C', 'A', '$'};
    char t[] = {L_TYPE, S_TYPE, L_TYPE, S_TYPE, L_TYPE, L_TYPE, S_TYPE,  L_TYPE,  L_TYPE, S_TYPE, L_TYPE, L_TYPE, L_TYPE, S_TYPE};
    int SA[] = {4, 0, 1, 3, 2, 1, 2, -1, 1, 2, 4, 3, 0};
    int P[] = {1, 3, 6, 9, 12};
    int real_LCP[] = {0, 0, 1, 1, 0, 2, 1, 0, 2, 1, 0, 1, 2, 1};

    int n = sizeof(S) / sizeof(int);
    int n1 = sizeof(P) / sizeof(int);

    int *SA1 = SA;
    int *S1 = SA + n - n1;

    int* LCP = (int* ) malloc(n * sizeof(int));
    int* LCP_map = (int* ) malloc(n * sizeof(int));

    for (int i = 0; i < n; i++) {
        LCP[i] = -2;
        LCP_map[i] = -1;
    }

    LCP_map[1] = 1;
    LCP_map[3] = 0;
    LCP_map[6] = 0;
    LCP_map[9] = 0;
    LCP_map[12] = 0;

    int bucket_size = 0;

    for (long i = 0; i < n; i++) {
        if(S[i] > bucket_size) bucket_size = S[i];
    }

    int B[++bucket_size];

    induced_sorting_with_lcp(S, SA, S1, SA1, LCP, t, LCP_map, n, n1, bucket_size, B);

    for(int i = 0; i < n; i++) {
        ASSERT_EQ(real_LCP[i], LCP[i]);
    }
}

TEST(determine_lms_indices_tests, jednostavni_primjer) {
    char t[] = {L_TYPE, S_TYPE, L_TYPE, S_TYPE, L_TYPE, L_TYPE, S_TYPE,  L_TYPE,  L_TYPE, S_TYPE, L_TYPE, L_TYPE, L_TYPE, S_TYPE};
    int real_p[] = {1, 3, 6, 9, 13};

    int n = sizeof(t) / sizeof(char);
    int n1 = sizeof(real_p) / sizeof(int);
    int p[n1];

    determine_lms_indices(t, p, n);

    for(int i = 0; i < n1; i++) {
        ASSERT_EQ(real_p[i], p[i]);
    }
}

TEST(naive_lcp_tests, simple_example) {
    const int S[] = {'C', 'A', 'T', 'G', 'T', 'G', 'C', 'T', 'T', 'A', 'G', 'C', 'A', '$'};

    int n = sizeof(S) / sizeof(int);
    int q = 4;
    int p = 7;

    int real_lcp = 1;
    int lcp = naive_lcp(S, n, q, p);

    ASSERT_EQ(real_lcp, lcp);

    q = 0;
    p = 3;

    real_lcp = 0;
    lcp = naive_lcp(S, n, q, p);

    ASSERT_EQ(real_lcp, lcp);
}

TEST(naive_min_tests, simple_example) {
    int LCP[] = {-1, 0, 0, 1, 1, 2, 1, -1, 0, 3};
    int start = 0;
    int finish = 3;
    int real_min = 0;

    int min = naive_min(LCP, start, finish);
    ASSERT_EQ(real_min, min);

    start = 3;
    finish = 6;
    real_min = 1;

    min = naive_min(LCP, start, finish);
    ASSERT_EQ(real_min, min);

    start = 9;
    finish = 9;
    real_min = 3;

    min = naive_min(LCP, start, finish);
    ASSERT_EQ(real_min, min);
}


TEST(determine_lcp_values_for_lms_tests, simple_example) {
    const int S[] = {'C', 'A', 'T', 'G', 'T', 'G', 'C', 'T', 'T', 'A', 'G', 'C', 'A', '$'};
    int P[] = {1, 3, 6, 9, 13};
    int SA1[] = {4, 3, 0, 2, 1};
    int real_LCP[] = {0, 0, 1, 0, 0};

    int n = sizeof(S) / sizeof(int);
    int n1 = sizeof(P) / sizeof(int);
    int LCP[n1];

    determine_lcp_values_for_lms(SA1, LCP, S, P, n, n1);

    for(int i = 0; i < n1; i++) {
        ASSERT_EQ(real_LCP[i], LCP[i]);
    }
}

TEST(SA_IS_tests, simple_example) {
    const int S[] = {'C', 'A', 'T', 'G', 'T', 'G', 'C', 'T', 'T', 'A', 'G', 'C', 'A', '$'};
    int real_SA[] = {13, 12, 9, 1, 11, 0, 6, 10, 5, 3, 8, 4, 2, 7};
    int real_LCP[] = {0, 0, 1, 1, 0, 2, 1, 0, 2, 1, 0, 1, 2, 1};

    int n = sizeof(S) / sizeof(int);
    int* SA = (int* ) malloc(n * sizeof(int));
    int* LCP = (int* ) malloc(n * sizeof(int));
    int calculate_lcp = 1;

    for(int i = 0; i < n; i++) {
        LCP[i] = -1;
    }

    int bucket_size = 0;

    for (long i = 0; i < n; i++) {
        if(S[i] > bucket_size) bucket_size = S[i];
    }

    SA_IS(S, SA, LCP, n, bucket_size + 1, calculate_lcp);

    for(int i = 0; i < n; i++) {
        ASSERT_EQ(real_SA[i], SA[i]);
        ASSERT_EQ(real_LCP[i], LCP[i]);
    }
}
