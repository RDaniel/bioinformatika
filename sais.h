#ifndef BIOINFORMATIKA_SAIS_H
#define BIOINFORMATIKA_SAIS_H

#define L_TYPE 0
#define S_TYPE 1
#define isLMS(i) (i > 0 && t[i] && !t[i - 1])

const int NUM_OF_SYMBOLS = 4;

void assign_type(const int *S, long n, char *t);
void set_buckets_indexes(const int *S, int *B, long n, int bucket_size, bool end);
int induced_sorting(const int *S, int *SA, const char *t, long n, int *B, int bucket_size);
int determine_lms_names(const char *t, const int *S, int *SA, const int n);
void determine_SA_from_SA1(const int *S, int *SA, int *S1, int *SA1, int *B, const char *t, int n, int n1, int bucket_size);
void induced_sorting_with_lcp(const int *S, int *SA, int *S1, int *SA1, int *LCP, const char *t, const int *LCP_map, int n, int n1, int bucket_size, int *B);
void determine_lms_indices(const char *t, int *P, int n);
int naive_lcp(const int *S, int n, int q, int p);
int naive_min(int *LCP, int start, int finish);
void determine_lcp_values_for_lms(int *SA1, int *LCP, const int *S, int *P, int n, int n1);
void SA_IS(const int *S, int *SA, int *LCP, const int n, int bucket_size, const int calculate_lcp);

#endif
