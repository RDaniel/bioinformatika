#include "sais.h"
#include <stdlib.h>

/**
	Valentina Fatorić
*/
void assign_type(const int *S, long n, char *t) {
    t[n - 1] = S_TYPE;
    t[n - 2] = L_TYPE;

    for (long i = n - 3; i >= 0; i--) {
        if (S[i] < S[i + 1] || (S[i] == S[i + 1] && t[i + 1] == S_TYPE)) {
            t[i] = S_TYPE;
        } else {
            t[i] = L_TYPE;
        }
    }
}

/**
	Valentina Fatorić
*/
void set_buckets_indexes(const int *S, int *B, long n, int bucket_size, bool end) {
    for (int i = 0; i < bucket_size; i++) {
        B[i] = 0;
    }

    for (int i = 0; i < n; i++) {
        B[S[i]]++;
    }

    int sum = 0;

    for (int i = 0; i < bucket_size; i++) {
        if (B[i] == 0) {
            B[i] = -1;
            continue;
        }

        sum += B[i];

        // Setting bucket index at the end or beginning of bucket
        B[i] = end ? sum - 1 : sum - B[i];
    }
}

/**
	Valentina Fatorić
*/
int induced_sorting(const int *S, int *SA, const char *t, long n, int *B, int bucket_size) {
	int n1 = 0;

    for (int i = 0; i < n; i++) {
        SA[i] = -1;
    }

    // Add LMS indexes to SA at the end of buckets
    set_buckets_indexes(S, B, n, bucket_size, true);
    for (int i = 1; i < n; i++) {
        if isLMS(i) {
            SA[B[S[i]]--] = i;
			n1++;
        }
    }

    // Add L-types to SA at the beginning of buckets
    set_buckets_indexes(S, B, n, bucket_size, false);
    for (int i = 0; i < n; ++i) {
        if (SA[i] > 0) {
            if (t[SA[i] - 1] == L_TYPE) {
                SA[B[S[SA[i] - 1]]] = SA[i] - 1;
                B[S[SA[i] - 1]]++;
            }
        }
    }

    // Add S-types to SA at the end of buckets
    set_buckets_indexes(S, B, n, bucket_size, true);
    for (int i = n - 1; i >= 0; --i) {
        if (SA[i] > 0) {
            if (t[SA[i] - 1] == S_TYPE) {
                SA[B[S[SA[i] - 1]]] = SA[i] - 1;
                B[S[SA[i] - 1]]--;
            }
        }
    }

	return n1;
}

/**
	Luka Banović
*/
int determine_lms_names(const char *t, const int *S, int *SA, const int n){
	int n1 = 0;

	// Storing LMS indexes into start of SA
    for (int i = 0; i < n; i++)
        if isLMS(SA[i]) SA[n1++] = SA[i];
    for (int i = n1; i < n; i++){
        SA[i] = -1;
	}

	int name = -1;
	int former = -1;

	for(int i = 0; i < n1; i++){
		int current = SA[i];
		bool different = false;
		for (int j = 0; j < n; j++){
			// Detect if the neighbouring LMS substring is different than the current one
			if (former == -1 || S[current + j] != S[former + j] || t[current + j] != t[former + j]){
				different = true;
				break;
			}
			if (j > 0 && (isLMS(current + j) || isLMS(former + j))){
				break;
			}
		}
		if (different){
			name++;
		}
		// Names can be stored into SA as well since there are less than half of them. We will sort them by index.
		int position = current / 2;
		SA[n1 + position] = name;
		former = current;
	}
	// Names are pushed to into last n1 places in SA
    for (int i = n - 1, j = n - 1; i >= n1; i--){
        if (SA[i] >= 0) SA[j--] = SA[i];
	}

	return name;
}

/**
	Daniel Rakovac
*/
void determine_lms_indices(const char *t, int *P, int n) {
    for (int i = 0, j = 0; i < n; i++) if isLMS(i) P[j++] = i;
}

/**
	Luka Banović
*/
void determine_SA_from_SA1(const int *S, int *SA, int *S1, int *SA1, int *B, const char *t, int n, int n1, int bucket_size){
	set_buckets_indexes(S, B, n, bucket_size, true);

	// Store LMS indexes and order them lexicographically
	for (int i = 1, j = 0; i < n; i++)
        if (isLMS(i)) S1[j++] = i;

	for (int i = 0; i < n1; i++) SA1[i] = S1[SA1[i]];
    for (int i = n1; i < n; i++) SA[i] = -1;

	for (int i = n1 - 1; i >= 0; i--) {
        int j = SA[i];
        SA[i] = -1;
        SA[B[S[j]]] = j;
        B[S[j]]--;
    }

	set_buckets_indexes(S, B, n, bucket_size, false);
	for (int i = 0; i < n; ++i) {
        if (SA[i] > 0) {
            if (t[SA[i] - 1] == L_TYPE) {
                SA[B[S[SA[i] - 1]]] = SA[i] - 1;
                B[S[SA[i] - 1]]++;
            }
        }
    }

	set_buckets_indexes(S, B, n, bucket_size, true);
    for (int i = n - 1; i >= 0; --i) {
        if (SA[i] > 0) {
            if (t[SA[i] - 1] == S_TYPE) {
                SA[B[S[SA[i] - 1]]] = SA[i] - 1;
                B[S[SA[i] - 1]]--;
            }
        }
    }
}

/**
	Valentina Fatorić
*/
void determine_lcp_values_for_lms(int *SA1, int *LCP, const int *S, int *P, int n, int n1) {
    int *compare_with = (int *) malloc(n1 * sizeof(int));
    int *LMS_LCP = (int *) malloc(n1 * sizeof(int));
    int j = SA1[0];
    compare_with[j] = n - 1;

    for (int i = 1; i < n1; i++) {
        int q = SA1[i];
        compare_with[q] = P[j];
        j = q;
    }

    int lms_lcp = 0;
    j = 0;
    for (int i = 0; i < n; i++) {
        if (i != P[j]) continue;
        lms_lcp = 0;
        while ((i + lms_lcp < n && compare_with[j] + lms_lcp < n)) {
            if (S[i + lms_lcp] == S[compare_with[j] + lms_lcp] && i != n - 1) lms_lcp++;
            else break;
        }
        LMS_LCP[j++] = lms_lcp;
    }

    for (j = 0; j < n1; ++j) LCP[j] = LMS_LCP[SA1[j]];

    free(compare_with);
    free(LMS_LCP);
}

/**
	Daniel Rakovac
*/
int naive_lcp(const int *S, int n, int q, int p) {
    int i;
    for (i = 0; i < n; i++) if (S[p + i] != S[q + i]) break;
    return i;
}

/**
	Daniel Rakovac
*/
int naive_min(int *LCP, int start, int finish) {
    int min = start;
    for (int i = start + 1; i <= finish; i++) {
        if (LCP[min] < 0 || (LCP[i] > -1 && LCP[i] < LCP[min])) min = i;
    }
    return LCP[min];
}

/**
	Daniel Rakovac
*/
void induced_sorting_with_lcp(const int *S, int *SA, int *S1, int *SA1, int *LCP, const char *t, const int *LCP_map, int n, int n1, int bucket_size, int *B){
    set_buckets_indexes(S, B, n, bucket_size, true);
    for (int i = 1, j = 0; i < n; i++)
        if (isLMS(i)) S1[j++] = i;

    for (int i = 0; i < n1; i++) SA1[i] = S1[SA1[i]];
    for (int i = n1; i < n; i++) SA[i] = -1;

    for (int i = n1 - 1; i >= 0; i--) {
        int j = SA[i];
        SA[i] = -1;
        SA[B[S[j]]] = j;
        LCP[B[S[j]]] = LCP_map[j];
        B[S[j]]--;
    }

    int stems[NUM_OF_SYMBOLS];
    for (int i = 0; i < NUM_OF_SYMBOLS; i++) stems[i] = -1;
    for (int i = 0, j = 0; i < bucket_size; i++) {
        if (B[i] >= 0 && B[i] + 1 < n && SA[B[i] + 1] >= 0) {
            int k = i + 1;
            while (B[k] == -1 && k++ < bucket_size);
            if (k < bucket_size && B[i] + 1 < B[k]) {
                LCP[B[i] + 1] = -1;
                stems[j++] = B[i] + 1;
            }
            i = k - 1;
        }
    }

    set_buckets_indexes(S, B, n, bucket_size, false);
    int last_placed[bucket_size];
    for (int i = 0; i < bucket_size; i++)
        last_placed[i] = -1;

    for (int i = 0; i < n; ++i) {
        if (LCP[i] == -1) {
            if (SA[i] >= 0 && last_placed[S[SA[i]]] >= 0)
                LCP[i] = naive_lcp(S, n, SA[i], SA[last_placed[S[SA[i]]]] - 1);
             else
                LCP[i] = 0;
        }
        if (SA[i] > 0 && t[SA[i] - 1] == L_TYPE) {
            int symbol = S[SA[i] - 1];
            int k = B[symbol];
            int j = last_placed[symbol];
            SA[k] = SA[i] - 1;
            B[symbol]++;
            if (j == -1)
                LCP[k] = 0;
            else if (S[SA[i]] != S[SA[j]])
                LCP[k] = 1;
            else {
                int l = naive_min(LCP, j + 1, i);
                LCP[k] = l + 1;
            }
            last_placed[symbol] = i;
        }
    }

    for (int stem : stems) {
        if (stem >= 0) LCP[stem] = -1;
    }

    set_buckets_indexes(S, B, n, bucket_size, true);
    for (int i = 0; i < bucket_size; i++)
        last_placed[i] = -1;

    for (int i = n - 1; i >= 0; --i) {
        if (LCP[i] < 0 && LCP[i - 1] >= 0) {
            LCP[i] = naive_lcp(S, n, SA[i], SA[i - 1]);
        }
        if (SA[i] > 0 && t[SA[i] - 1] == S_TYPE) {
            int symbol = S[SA[i] - 1];
            int k = B[symbol];
            SA[k] = SA[i] - 1;
            B[symbol]--;
            if (last_placed[symbol] != -1) {
                int j = last_placed[symbol];
                if (S[SA[i]] != S[SA[j]])
                    LCP[k + 1] = 1;
                else {
                    int l = naive_min(LCP, i + 1, j);
                    LCP[k + 1] = l + 1;
                }
            }
            last_placed[symbol] = i;
        }
    }

    for (int i = 0; i < n; i++) if (LCP[i] < 0) LCP[i] = 0;
}

/**
	Luka Banović
*/
void SA_IS(const int *S, int *SA, int* LCP, const int n, int bucket_size, const int calculate_lcp){
    char *t = (char *) malloc(n * sizeof(char));
    assign_type(S, n, t);

    int *B = (int *) malloc(bucket_size * sizeof(int));
    int n1 = induced_sorting(S, SA, t, n, B, bucket_size);

    int name = determine_lms_names(t, S, SA, n);

    int *SA1 = SA;
    int *S1 = SA + n - n1;

    if (name != n1 - 1) {
        SA_IS(S1, SA1, LCP, n1, name + 1, 0);
    } else {
        for (int i = 0; i < n1; i++) SA1[S1[i]] = i;
    }

    if(calculate_lcp) {
        int *LCP_map = (int *) malloc(n * sizeof(int));
        int *P = (int *) malloc(n1 * sizeof(int));
        determine_lms_indices(t, P, n);
        determine_lcp_values_for_lms(SA1, LCP, S, P, n, n1);

        for (int i = 0; i < n1; i++) {
            LCP_map[P[SA1[i]]] = LCP[i];
            LCP[i] = -2;
        }

        induced_sorting_with_lcp(S, SA, S1, SA1, LCP, t, LCP_map, n, n1, bucket_size, B);

        free(LCP_map);
        free(P);
    } else {
        determine_SA_from_SA1(S, SA, S1, SA1, B, t, n, n1, bucket_size);
    }

    free(t);
    free(B);
}















